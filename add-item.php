<?php 
	require "template/template.php";

	function getContent(){
?>

<!-- content here -->
<h1 class="text-center py-5">Add Item Form</h1>
<div class="d-flex justify-content-center align-items-center">
	<!-- we need to remember if we want to capture data from an input type file, we need to add the attribute the enctype="multipart/form-data" in our form tag -->
	<form action="controllers/process_add_item.php" method="POST" class="pb-5" enctype="multipart/form-data">
		<div class="form-group">
			<label for="name">Item Name</label>
			<input type="text" name="name" class="form-control" placeholder="Input item Name">
		</div>
		<div class="form-group">
			<label for="price">Item Price:</label>
			<input type="number" name="price" class="form-control" placeholder="Input item price">
		</div>
		<div class="form-group">
			<label for="quantity">Item Quantity:</label>
			<input type="number" name="quantity" class="form-control">
			
		</div>
		<div class="form-group">
			<label for="description">Item Description:</label>
			<textarea name="description" class="form-control"></textarea>
		</div>
		<!-- for image -->
		<div class="form-group">
			<label for="imgPath">Item Image:</label>
			<input type="file" name="imgPath" class="form-control">
			
		</div>
		<!-- for category -->

		
		<div class="form-group">
			<label for="category_id">Category:</label>
			<select name="category_id" class="form-control">
				<?php 
					require "controllers/connection.php";

					$category_query = "SELECT * FROM categories";

					$categories = mysqli_query($conn, $category_query);

					foreach($categories as $indivCategory){
						?>
						<!-- html area -->
							<option value="<?php echo $indivCategory['id'] ?>">
								<?php echo $indivCategory['name'] ?>
							</option>
						<?php
					}
				 ?>
			</select>
		</div>
		<button class="btn btn-info" type="submit">Add Item</button>

	</form>

</div> 



<?php
	}


?>