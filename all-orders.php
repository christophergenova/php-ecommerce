<?php 
	require "template/template.php";

	function getTitle(){
		echo "BrandoStore | All Orders";
	}

	function getContent(){
		require "controllers/connection.php";
		?>

	<h1 class="text-center py-5">All Orders</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<table class="table table-striped border">
					<thead>
						<tr>
							<th>Order Id</th>
							<th>Order Date</th>
							<th>Order Details</th>
							<th>Total</th>
							<th>Status</th>
							<th>Payment</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php 
						// we need to get the order id, order date, total, status_id -> name, payment_id -> name.
						// notice that we skipped order details where we will publish the item name and the quantity.
							$user_id = $_SESSION['user']['id'];
						// in my orders query, we joined orders, statuses and payments.
							$orders_query = "SELECT statuses.name as status, payments.name as payment, total, orderDate, orders.id as order_id FROM orders JOIN statuses ON (statuses.id = orders.status_id) JOIN payments ON (payments.id = orders.payment_id)";
							//this query will result to an array of orders
							$orders = mysqli_query($conn, $orders_query);
						
							foreach($orders as $indiv_order){
						?>
						<tr>
							<td><?php echo $indiv_order['order_id']; ?></td>
							<td><?php echo $indiv_order['orderDate']; ?></td>
							<td>
								<?php
								//this came from our first query 
									$order_id = $indiv_order['order_id'];
									// to get the order details (item name and quantity), we joined items and item_order.
									$items_query = "SELECT items.name as item_name, item_order.quantity as quantity FROM item_order JOIN items ON (items.id = item_order.item_id) WHERE order_id = $order_id";
									// this will result to an array of items
									$items = mysqli_query($conn, $items_query);
								
									foreach($items as $indiv_item){
								?>
									<span><?php echo $indiv_item['quantity'] . " - " . $indiv_item['item_name'] ?></span><br>
								<?php
									}
								 ?>
							</td>
							<td><?php echo $indiv_order['total']; ?></td>
							<td><?php echo $indiv_order['status']; ?></td>
							<td><?php echo $indiv_order['payment'] ?></td>
							<td>
								<!-- previous place of cancel button -->
									<a href="controllers/process_cancel_order.php?order_id=<?php echo $indiv_order['order_id']; ?>" class="btn btn-danger">Cancel</a>
								<?php 
								//status_id --> is now status since we joined the orders table with statuses table
									if($indiv_order['status']!=="cancelled"){
								?>
									<a href="controllers/process_deliver_order.php?order_id=<?php echo $indiv_order['order_id']; ?>" class="btn btn-primary">Mark as Delivered</a>
								<?php
									}
								 ?>
							</td>
						</tr>	
						<?php
							}
						 ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php
	}
 ?>
