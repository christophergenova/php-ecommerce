<?php 

	require "template/template.php";

	function getContent(){
	?>

	<div class="d-flex justify-content-center align-items-center flex-column">
		<h1 class="py-5">Login</h1>

		<form action="controllers/process_login.php" method="POST">
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" name="email" class="form-control" id="email">
				<span class="text-danger"></span>
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" name="password" class="form-control" id="password">
				<span class="text-danger"></span>
			</div>
			<button type="button" class="btn btn-primary" id="login">Login</button>
		</form>
		<p class="py-3">New User? <a href="register.php">Register</a></p>
	</div>

	<?php
	}


?>