const login = document.getElementById('login');


const emailInputs = document.getElementById('email');
const passwordInputs = document.getElementById('password');

function isEmpty(value, element){
    if(value === ""){
        element.nextElementSibling.textContent = "This field is required.";
        return true;
    }else{
        element.nextElementSibling.textContent = "";
        return false;
    }
}

emailInputs.addEventListener('blur', function(){
    const emailValue = emailInputs.value;
    isEmpty(emailValue, emailInputs);
    
});

passwordInputs.addEventListener('blur', function(){
	const passwordValue = passwordInputs.value;
	isEmpty(passwordValue, passwordInputs);
});


login.addEventListener('click', function(){
    //This will serve as our form data container. Since we disabled the form submission in our frontend, we need a
    // container to hold the data that can be accessed by $_POST
    let data = new FormData;

    //to add a data in our newly created FormData, use append method, the first arg is the name which is equivalent
    // to the name attribute of input, and the second arg is the value

    data.append('email', emailInputs.value);
    data.append('password', passwordInputs.value);

    //if we will not get data (GET)
    //since we want to add a data in our database, the second argument will be an object containing the method and
    // the data we want to add or process
    fetch('../../controllers/process_login.php', {
        method: "POST",
        body: data,
    }).then(function(response){
        //the response parameter is the response from the fetch.
        //we need to transform it into a data format that we can use, for this instance, we transformed it into a
        // text format.
        return response.text();
    }).then(function(response_from_fetch){
        //the response_from_fetch is the data we got from the first then which is the response.text()
        console.log(response_from_fetch);
        if(response_from_fetch === "Failed"){
            toastr['warning']("Wrong username or password!");
        }else{
            //we are redirecting to the login page
            window.location.replace("../../index.php");
        }
    })
});