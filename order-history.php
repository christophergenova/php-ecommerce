<?php 
	require "template/template.php";



	function getContent(){
		require "controllers/connection.php";
		?>

	<h1 class="text-center py-5">Order History</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<table class="table table-striped border">
					<thead>
						<tr>
							<th>Order Id</th>
							<th>Order Date</th>
							<th>Order Details</th>
							<th>Total</th>
							<th>Status</th>
							<th>Payment</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$user_id = $_SESSION['user']['id'];
							$orders_query = "SELECT statuses.name as status, payments.name as payment, total, orderDate, orders.id as order_id FROM orders JOIN statuses ON (statuses.id = orders.status_id) JOIN payments ON (payments.id = orders.payment_id) WHERE user_id = $user_id";
							$orders = mysqli_query($conn, $orders_query);
						
							foreach($orders as $indiv_order){
						?>
						<tr>
							<td><?php echo $indiv_order['order_id']; ?></td>
							<td><?php echo $indiv_order['orderDate']; ?></td>
							<td>
								<?php 
									$order_id = $indiv_order['order_id'];
									$items_query = "SELECT items.name as item_name, item_order.quantity as quantity FROM item_order JOIN items ON (items.id = item_order.item_id) WHERE order_id = $order_id";

									$items = mysqli_query($conn, $items_query);
								
									foreach($items as $indiv_item){
								?>
									<span><?php echo $indiv_item['quantity'] . " - " . $indiv_item['item_name'] ?></span><br>
								<?php
									}
								 ?>
							</td>
							<td><?php echo $indiv_order['total']; ?></td>
							<td><?php echo $indiv_order['status']; ?></td>
							<td><?php echo $indiv_order['payment'] ?></td>
							<td><a href="controllers/process_cancel_order.php?order_id=<?php echo $indiv_order['order_id'] ?>" class="btn btn-danger">Cancel</a></td>
							
						</tr>	
						<?php
							}
						 ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php
	}
 ?>
