<?php 
	require "template/template.php";

	function getContent(){

		require "controllers/connection.php";
		// get the item id
		$user_id = $_SESSION['user']['id'];

		// $editProfile = "SELECT * FROM users join profiles on (profiles.user_id = users.id)";

		// $editFull = mysqli_fetch_assoc(mysqli_query($conn, $editProfile));
		// ----------------------------
		
		$profileViewer = "SELECT * FROM profiles WHERE user_id = $user_id";
		$userViewer = "SELECT * FROM users WHERE id = $user_id";

		$superViewer = mysqli_fetch_assoc(mysqli_query($conn, $profileViewer));
		$superViewer2 = mysqli_fetch_assoc(mysqli_query($conn, $userViewer));


		// $user_query = "SELECT * FROM users WHERE id = $user_id";
		// // transofrm the result form the query
		// $edituser = mysqli_fetch_assoc(mysqli_query($conn, $user_query));


		// $prof_query = "SELECT * FROM profiles WHERE id = $user_id";

		// $editprof = mysqli_fetch_assoc(mysqli_query($conn, $prof_query));


	
	?>

	<h1 class="text-center py-5">Edit Profile Form</h1>
	<div class="d-flex justify-content-center align-items-center">
		<form action="controllers/process_edit_profile.php" method="POST" class="pb-5" enctype="multipart/form-data">
			<div class="form-group">
				<label for="firstname">First Name</label>
				<input type="text" name="firstname" class="form-control" value="<?php echo $superViewer2['firstname'] ?>">

			</div>
			<div class="form-group">
				<label for="lastname">Last Name</label>
				<input type="text" name="lastname" class="form-control" value="<?php echo $superViewer2['lastname'] ?>">

			</div>

			<div class="form-group">
				<label for="email">Email</label>
				<input type="text" name="email" class="form-control" value="<?php echo $superViewer2['email'] ?>">

			</div>

			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control" value="<?php echo $superViewer2['password'] ?>">

			</div>

			<div class="form-group">
				<label for="name">Address</label>
				<input type="text" name="address" class="form-control" value="<?php echo $superViewer['address'] ?>">

			</div>

			<div class="form-group">
				<label for="name">Contact no.</label>
				<input type="text" name="contactNo" class="form-control" value="<?php echo $superViewer['contactNo'] ?>">

			</div>

			<div class="form-group">
				<label for="profileImg">Profile picture</label>
				<input type="file" name="profileImg" class="form-control">

			</div>
			<button class="btn btn-info" type="submit">Edit Profile</button>
		</form>
	</div>	


	<?php

	}

?>	