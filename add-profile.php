<?php

	require "template/template.php";

	function getContent(){
	?>
	<div class="d-flex justify-content-center align-items-center flex-column">
		<h1 class="my-5">Add Profile</h1>
		<form action="controllers/process_add_profile.php" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label for="address">Address:</label>
				<input type="text" name="address" class="form-control">
			</div>
			<div class="form-group">
				<label for="contactNo">Contact Number</label>
				<input type="text" name="contactNo" class="form-control">
			</div>
			<div class="form-group">
				<label for="profileImg">Upload Photo</label>
				<input type="file" name="profileImg" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Add Profile</button>
			</div>
			

		</form>



	</div> 



	<?php
	}








 ?>