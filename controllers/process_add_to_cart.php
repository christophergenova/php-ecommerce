<?php 
	session_start();
	// capture the data we need
	// id of the item
	$item_id = $_POST['item_id'];

	// quantity of the item
	$quantity_from_form = $_POST['quantity'];
	// actual quantity
	$quantity_from_db = $_POST['quantity_from_db'];



	// we need to check if the quantity added to cart is <= the actual in our db
	// if not enough, error
	// if enough, proceed
	if($quantity_from_form > $quantity_from_db){
		die('Insufficient');
	}else{
		if(isset($_SESSION['cart'][$item_id])){
		// we need to check if the total of the quantity in the session is not more than in the quantity in our db
		if($_SESSION['cart'][$item_id] + $quantity_from_form > $quantity_from_db){
			die("Insufficient stocks");
		}else{
			$_SESSION['cart'][$item_id] += $quantity_from_form;
		}
			
		
		}else{
			$_SESSION['cart'][$item_id] = $quantity_from_form;
		}
	}

	// header("Location: ../index.php");


?>