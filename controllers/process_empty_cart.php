<?php
	
	// the goal is to remove the contents of the variable $_SESSION['cart'];

	session_start();

	// to reset the value of the variable to null
	unset($_SESSION['cart']);

	//redirect to cart page;

	header("Location: " . $_SERVER['HTTP_REFERER']);



 ?>