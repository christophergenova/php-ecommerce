<?php 
	// goal is to remove an item from the variable $_SESSION['cart'];

	// we need an identifier to know which item we will remove

	session_start();

	$item_id = $_GET['item_id'];

	unset($_SESSION['cart'][$item_id]);

	header("Location: " . $_SERVER['HTTP_REFERER']);


?>