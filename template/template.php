<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://bootswatch.com/4/pulse/bootstrap.css">
	

	<!-- Jquery -->
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" defer></script>
	<!-- If a file is a dependency of another file, it should be at the top -->
	<!-- toastr JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" defer></script>

	<!-- Registration validation script -->
	<script src="../assets/scripts/register.js" defer>
	</script>
	<!-- Login validation -->
	<script src="../assets/scripts/login.js" defer>
	</script>

	<!-- add to cart -->
	<script src="../assets/scripts/addToCart.js" defer></script>
	<!-- Toastr css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	
	<title>Chris Vapeshop</title>
</head>
<body>
<?php require "navbar.php"; ?>

<!-- This is where we want to insert the content of the pages -->
<!-- We will create a function in every page, to get the content of that page -->
<?php 
	getContent();
	
?>


<?php require "footer.php"; ?>



</body>
</html>